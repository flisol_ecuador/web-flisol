import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

import { siteConfig } from '@/config';

import { Button, Icons } from '.';

export interface SiteFooterProps {}

const SiteFooter: React.FC<SiteFooterProps> = () => {
  return (
    <footer className='py-6 md:px-8'>
      <div className='container flex flex-col items-center justify-between gap-4 md:h-24 text-muted-foreground'>
        <div className='space-x-2'>
          <Link href={siteConfig.links.twitter} target='_blank'>
            <Button
              variant='secondary'
              size='icon'
              className='rounded-full text-current'
            >
              <Icons.twitter className='w-4 h-4' />
            </Button>
          </Link>

          <Link href={siteConfig.links.facebook} target='_blank'>
            <Button
              variant='secondary'
              size='icon'
              className='rounded-full text-current'
            >
              <Icons.facebook className='w-4 h-4' />
            </Button>
          </Link>

          <Link href={siteConfig.links.instagram} target='_blank'>
            <Button
              variant='secondary'
              size='icon'
              className='rounded-full text-current'
            >
              <Icons.instagram className='w-4 h-4' />
            </Button>
          </Link>

          <Link href={siteConfig.links.gitlab} target='_blank'>
            <Button
              variant='secondary'
              size='icon'
              className='rounded-full text-current'
            >
              <Icons.gitlab className='w-4 h-4' />
            </Button>
          </Link>
        </div>

        <Image
          src='/img/cc-license.png'
          alt='Creative Commons License'
          width='0'
          height='0'
          sizes='100vw'
          className='w-16 h-auto'
        />

        <p className='text-balance text-center text-xs -mt-2'>
          {siteConfig.subtitle}
        </p>
      </div>
    </footer>
  );
};

export { SiteFooter };
