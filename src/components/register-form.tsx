'use client';

import React from 'react';

export interface RegisterFormProps {}

const RegisterForm: React.FC<RegisterFormProps> = () => {
  return (
    <iframe
      src='https://openlab.ec/webform/convocatoria_abierta_para_ponen/share'
      title='Convocatoria abierta para participar en el FLISoL 2024 | Fundación Openlab Ecuador'
      className='webform-share-iframe w-full h-[1300px]'
      allowFullScreen
    />
  );
};

export { RegisterForm };
