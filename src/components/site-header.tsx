import Link from 'next/link';
import React from 'react';

import { Icons, MainNav, MobileNav, ModeToggle } from '.';

export interface SiteHeaderProps {}

const SiteHeader: React.FC<SiteHeaderProps> = () => {
  return (
    <header className='sticky top-0 z-50 w-full border-b border-border/40 bg-background/95 backdrop-blur supports-[backdrop-filter]:bg-background/60'>
      <div className='container flex h-14 max-w-screen-2xl items-center'>
        <Link href='/'>
          <Icons.logo className='w-16 h-auto' />
        </Link>

        <div className='flex flex-1 items-center justify-between space-x-2 md:justify-end'>
          <div className='w-full flex-1 md:w-auto md:flex-none'>
            <MainNav />
          </div>
          <ModeToggle />
          <MobileNav />
        </div>
      </div>
    </header>
  );
};

export { SiteHeader };
