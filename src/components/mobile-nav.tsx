'use client';

import React from 'react';

import { linksConfig } from '@/config';

import {
  Button,
  Icons,
  MobileLink,
  ScrollArea,
  Sheet,
  SheetContent,
  SheetHeader,
  SheetTrigger,
} from '.';

export interface MobileNavProps {}

const MobileNav: React.FC<MobileNavProps> = () => {
  const [open, setOpen] = React.useState(false);

  return (
    <Sheet open={open} onOpenChange={setOpen}>
      <SheetTrigger asChild>
        <Button
          variant='ghost'
          className='px-0 text-base hover:bg-transparent focus-visible:bg-transparent focus-visible:ring-0 focus-visible:ring-offset-0 md:hidden'
        >
          <Icons.bars className='h-5 w-5' />
          <span className='sr-only'>Toggle Menu</span>
        </Button>
      </SheetTrigger>

      <SheetContent side='right' className='pl-0'>
        <SheetHeader className='items-center'>
          <MobileLink href='/' className='h-auto w-20' onOpenChange={setOpen}>
            <Icons.logo />
          </MobileLink>
        </SheetHeader>

        <ScrollArea className='my-4 h-[calc(100vh-8rem)] pb-10 pl-6'>
          <div className='flex flex-col space-y-3'>
            {linksConfig.mainNav?.map(
              (item) =>
                item.href && (
                  <MobileLink
                    key={item.href}
                    href={item.href}
                    onOpenChange={setOpen}
                  >
                    {item.title}
                  </MobileLink>
                )
            )}
          </div>
        </ScrollArea>
      </SheetContent>
    </Sheet>
  );
};

export { MobileNav };
