import React from 'react';
import Balancer from 'react-wrap-balancer';

import { cn } from '@/lib';

export interface PageHeaderWrapperProps
  extends React.HTMLAttributes<HTMLDivElement> {}

const PageHeaderWrapper: React.FC<PageHeaderWrapperProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <div className={cn('flex flex-col items-center', className)} {...rest} />
  );
};

export interface PageHeaderProps extends React.HTMLAttributes<HTMLDivElement> {}

const PageHeader: React.FC<PageHeaderProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <section
      className={cn(
        'mx-auto flex max-w-[980px] flex-col items-center gap-2 py-8 md:py-12 md:pb-8 lg:py-24 lg:pb-20',
        className
      )}
      {...rest}
    />
  );
};

export interface PageHeaderHeadingProps
  extends React.HTMLAttributes<HTMLHeadingElement> {}

const PageHeaderHeading: React.FC<PageHeaderHeadingProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <Balancer
      as='h1'
      className={cn(
        'text-center text-3xl font-bold leading-tight tracking-tighter md:text-6xl lg:leading-[1.1]',
        className
      )}
      {...rest}
    />
  );
};

export interface PageHeaderSubHeadingProps
  extends React.HTMLAttributes<HTMLHeadingElement> {}

const PageHeaderSubHeading: React.FC<PageHeaderSubHeadingProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <Balancer
      as='h2'
      className={cn(
        'text-center text-2xl font-medium leading-tight tracking-tighter md:text-6xl lg:leading-[1.1]',
        className
      )}
      {...rest}
    />
  );
};

export interface PageHeaderDescriptionProps
  extends React.HTMLAttributes<HTMLParagraphElement> {}

const PageHeaderDescription: React.FC<PageHeaderDescriptionProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <Balancer
      className={cn(
        'max-w-[750px] text-center text-lg text-muted-foreground sm:text-xl',
        className
      )}
      {...rest}
    />
  );
};

export interface PageActionsProps
  extends React.HTMLAttributes<HTMLDivElement> {}

const PageActions: React.FC<PageActionsProps> = (props) => {
  const { className, ...rest } = props;

  return (
    <div
      className={cn(
        'flex w-full items-center justify-center space-x-4 py-4 md:pb-10',
        className
      )}
      {...rest}
    />
  );
};

export {
  PageActions,
  PageHeader,
  PageHeaderDescription,
  PageHeaderHeading,
  PageHeaderSubHeading,
  PageHeaderWrapper,
};
