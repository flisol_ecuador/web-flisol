import Link, { LinkProps } from 'next/link';
import { useRouter } from 'next/navigation';
import React from 'react';

import { cn } from '@/lib';

export interface MobileLinkProps extends LinkProps {
  onOpenChange?: (open: boolean) => void;
  children: React.ReactNode;
  className?: string;
}

const MobileLink: React.FC<MobileLinkProps> = (props) => {
  const { href, onOpenChange, className, children, ...rest } = props;

  const router = useRouter();

  return (
    <Link
      href={href}
      onClick={() => {
        router.push(href.toString());
        onOpenChange?.(false);
      }}
      className={cn(className)}
      {...rest}
    >
      {children}
    </Link>
  );
};

export { MobileLink };
