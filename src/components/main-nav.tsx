'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import React from 'react';

import { linksConfig } from '@/config';
import { cn } from '@/lib';

export interface MainNavProps {}

const MainNav: React.FC<MainNavProps> = () => {
  const pathname = usePathname();

  return (
    <nav className='hidden md:flex items-center gap-6 text-sm'>
      {linksConfig.mainNav?.map(
        (item) =>
          item.href && (
            <Link
              key={item.href}
              href={item.href}
              className={cn(
                'transition-colors hover:text-foreground/80',
                pathname === item.href
                  ? 'text-foreground'
                  : 'text-foreground/60'
              )}
            >
              {item.title}
            </Link>
          )
      )}
    </nav>
  );
};

export { MainNav };
