import { MainNavItem } from '@/types';

interface LinksConfig {
  mainNav: MainNavItem[];
}

export const linksConfig: LinksConfig = {
  mainNav: [
    { title: 'Inicio', href: '/' },
    { title: 'Registro', href: '/#registro' },
    { title: 'Canales', href: '/#canales' },
    { title: 'Sedes', href: '/#sedes' },
    { title: 'Código de Conducta', href: '/codigo' },
  ],
};
