export const siteConfig = {
  name: 'FLISoL Ecuador',
  subtitle: 'Festival Latinoamericano de Instalación de Software Libre',
  description:
    'El evento de difusión de Software Libre más grande en América Latina',
  links: {
    twitter: 'https://twitter.com/flisol',
    facebook: 'https://facebook.com/flisol',
    instagram: 'https://instagram.com/flisol',
    gitlab: 'https://gitlab.com/flisol',
  },
};

export type SiteConfig = typeof siteConfig;
