import Image from 'next/image';

import {
  PageHeader,
  PageHeaderDescription,
  PageHeaderHeading,
  PageHeaderWrapper,
  RegisterForm,
} from '@/components';
import { siteConfig } from '@/config';

export default function Home() {
  return (
    <div className='relative'>
      <PageHeaderWrapper className='bg-secondary'>
        <PageHeader className='container'>
          <Image
            src='/img/flisol-banner.png'
            alt={siteConfig.name}
            width={0}
            height={0}
            sizes='100vw'
            className='w-full h-auto max-w-2xl'
          />
          <PageHeaderHeading>Charlas, Talleres, Install Fest</PageHeaderHeading>
          <PageHeaderDescription>
            {siteConfig.description}
          </PageHeaderDescription>
        </PageHeader>

        <Image
          src='/img/header-graphic.png'
          alt={siteConfig.name}
          width={0}
          height={0}
          sizes='100vw'
          className='w-full h-auto max-w-2xl'
        />
      </PageHeaderWrapper>

      <section className='container py-8'>
        <RegisterForm />
      </section>
    </div>
  );
}
